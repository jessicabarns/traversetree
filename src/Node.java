public class Node {
	public char data;
    public Node leftChild;
    public Node rightChild;
    
    public Node (char data, Node leftChild, Node rightChild) {
        this.leftChild = leftChild;
        this.rightChild = rightChild;
        this.data = data;
    }
    
    public Node (char data) {
        this.leftChild = null;
        this.rightChild = null;
        this.data = data;
    }
    
    public boolean isLeaf(){
    	return leftChild == null && rightChild == null;
    }
}
