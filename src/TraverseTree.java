import java.util.Stack;

public class TraverseTree {
    private Node node;
    private StringBuilder data;
    
    public TraverseTree (Node node) {
        this.node = node;
    }
    
    public String parcours(Node node){
    	if(node == null){
    		return "";
    	}
    	if(node.isLeaf()){
    		return node.data + "";
    	}
    	String result = "(";
    	result += parcours(node.leftChild);
    	result += node.data;
    	result += parcours(node.rightChild);
    	result += ")";
    	return result;
    }
    
    public String toString () {
    	return parcours(node);
        /*data = new StringBuilder();
        final Node parenthese = new Node(')');
        Stack<Node> operateurs = new Stack<Node>();
        
        Node currentNode = node;
        do {
            if (currentNode.isLeaf()) {
                addNumber(currentNode.data);
                
                do {
                    if (operateurs.isEmpty()){
                    	return data.toString();
                    }                    
                    currentNode = operateurs.pop();
                    addOperateur(currentNode.data);
                } while (currentNode == parenthese);
                
                currentNode = currentNode.rightChild;
            }
            else {
                data.append('(');
                operateurs.push(parenthese);
                operateurs.push(currentNode);
                currentNode = currentNode.leftChild;
            }
        } while (currentNode != null);*/
        
        //throw new RuntimeException("L'�quation � imprimer est invalide");
    }
    
    private void addNumber (char number) {
        if (Character.isDigit(number)){
        	data.append(number);
        }
        else{
        	throw new RuntimeException("Le caract�re " + number + " n'est pas un chiffre.");
        }            
    }
    
    private void addOperateur (char operateur) {
        if (!Character.isDigit(operateur)){
        	data.append(operateur);
        }            
        else{
        	 throw new RuntimeException("Le caract�re " + operateur + " est un chiffre.");
        }          
    }
}
