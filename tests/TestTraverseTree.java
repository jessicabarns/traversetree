import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.Test;

public class TestTraverseTree {
    private Node[] terminaux;
    private Node petit, moyen, grand;
    
    @Before
    public void genererExemples () {
        genererTerminaux();
        
        // (1+2)
        petit = new Node('+', terminaux[1], terminaux[2]);
        
        // ((1+2)*3)
        moyen = new Node('*', petit, terminaux[3]);
        
        // ((4-5)*((1+2)*3))
        grand = new Node('*', new Node('-', terminaux[4], terminaux[5]), moyen);
    }
    
    private void genererTerminaux () {
        terminaux = new Node[6];
        terminaux[0] = new Node('+'); // Node invalide
        for (byte i = 1; i <= 5; i++)
            terminaux[i] = new Node(Character.forDigit(i, 10));
    }
    
    @Test
    public void miniEquation () {
        TraverseTree tree = new TraverseTree(terminaux[1]);
        assertEquals("1", tree.toString());
    }
    
    @Test (expected = RuntimeException.class)
    public void miniIncompleteEquation () {
        TraverseTree tree = new TraverseTree(null);
        tree.toString();
    }    

    @Test (expected = RuntimeException.class)
    public void miniInvalidEquation () {
        TraverseTree tree = new TraverseTree(terminaux[0]);
        tree.toString();
    }
    
    @Test
    public void petiteEquation () {
        TraverseTree tree = new TraverseTree(petit);
        assertEquals("(1+2)", tree.toString());
    }
    
    @Test (expected = RuntimeException.class)
    public void petitIncompleteEquation () {
        petit.rightChild = null; // (1+null)
        TraverseTree tree = new TraverseTree(petit);
        tree.toString();
    }

    @Test (expected = RuntimeException.class)
    public void petitInvalidEquation1 () {
        terminaux[2].data = '*'; // (1+*)
        TraverseTree tree = new TraverseTree(petit);
        tree.toString();
    }
    
    @Test (expected = RuntimeException.class)
    public void petitInvalidEquation2 () {
        petit.data = '9'; // (192)
        TraverseTree tree = new TraverseTree(petit);
        tree.toString();
    }
    
    @Test
    public void moyenEquation () {
        TraverseTree tree = new TraverseTree(moyen);
        assertEquals("((1+2)*3)", tree.toString());
    }
    
    @Test (expected = RuntimeException.class)
    public void moyenIncompleteEquation () {
        petit.rightChild = null; // ((1+null)*3)
        TraverseTree tree = new TraverseTree(moyen);
        tree.toString();
    }
    
    @Test (expected = RuntimeException.class)
    public void moyenInvalidEquation1 () {
        terminaux[2].data = '*'; // ((1+*)*3)
        TraverseTree tree = new TraverseTree(moyen);
        tree.toString();
    }
    
    @Test (expected = RuntimeException.class)
    public void moyenInvalidEquation2 () {
        petit.data = '9'; // ((192)*3)
        TraverseTree tree = new TraverseTree(moyen);
        tree.toString();
    }
    
    @Test (timeout = 5)
    public void grandEquation () {
        TraverseTree tree = new TraverseTree(grand);
        assertEquals("((4-5)*((1+2)*3))", tree.toString());
    }
    
    @Test (timeout = 5)
    public void megaEquation () {
        petit.rightChild = new Node('*', new Node('6'), new Node('7'));
        moyen.rightChild = new Node('-', new Node('8'), new Node('9'));
        grand.leftChild.leftChild = new Node('*', new Node('2'), new Node('3'));
        TraverseTree tree = new TraverseTree(grand);
        assertEquals("(((2*3)-5)*((1+(6*7))*(8-9)))", tree.toString());
    }
}
